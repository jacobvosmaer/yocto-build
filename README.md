# Yocto v1 ATmega1284P build script

This collection of scripts lets you compile the firmware for the
[Yocto v1 from
e-licktronic](http://www.e-licktronic.com/en/content/25-yocto-tr808-clone-tr-808).
It uses Docker and it has been developed on Linux. It has been tested on macOS
and works there too.

It works on Windows too! See [user report on the Yocto
forum](http://www.e-licktronic.com/forum/viewtopic.php?f=17&t=1479&sid=c54f9ac75caebd50a443028ffe3c27d3#p9307).

## Installation

- Install [Docker](https://www.docker.com).
- Clone or download this repository.
- Obtain `Library.rar` and `Yocto_vXXX.rar` from the [e-licktronic
  forum](http://www.e-licktronic.com/forum/viewtopic.php?f=17&t=180&start=10)
- Extract the rar files in the directory tree of this repository
- Rename the `Yocto_vXXX` directory to `Yocto`

The directories should now look like this, with the `.ino` files in `Yocto`:

```
yocto-build
├── Library
│   ├── MIDI
│   ├── TR_SEQ
│   └── TimerOne
├── Yocto
└── bin
```

## How to use

When running the script for the first time, build the Docker container. The
Docker container will be built with Arduino software inside, plus patches to
the Wire library and support for the ATmega1284P as an Arduino 'board' using
[mighty-1284p](https://github.com/maniacbug/mighty-1284p).

```
# In the yocto-build directory
docker build -t yocto .
```

Then run the container with this repository mounted as a volume at `/yocto` in
the container. This will glue the Arduino files so they can be compiled by
[Arduino.mk](https://github.com/sudar/Arduino-Makefile). It then compiles the
Yocto software into a HEX file and a SYX file in the `build-mighty`
subdirectory.

```
# In the yocto-build directory
docker run -v /path/to/yocto-build:/yocto yocto
```

You can upload `yocto.syx` to your Yocto using the sysex firmware
update procedure (boot while holding 1-3-5, start sending sysex within
5 seconds).

Linux users can use the `bin/upload-sysex` script, but any other sysex
upload tool (like MidiOX) will work too.

## Disclaimer

I am not affiliated with e-licktronic in any way. Use these scripts at your own
risk.

## License

These scripts are distributed under the MIT license, see LICENSE.
